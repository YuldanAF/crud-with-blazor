#pragma checksum "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "01e848d8b48ca885cfc4533640be4180ef7bfc20"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorCrud.Client.Pages.Employee
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 2 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 3 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 4 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 6 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using BlazorCrud.Client;

#line default
#line hidden
#line 7 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using BlazorCrud.Client.Shared;

#line default
#line hidden
#line 8 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using BlazorCrud.Shared.Models;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.RouteAttribute("/employee/edit/{empID}")]
    public partial class EditEmployee : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h2>Edit</h2>\n");
            __builder.AddMarkupContent(1, "<h4>Employees</h4>\n<hr>\n");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "row");
            __builder.AddMarkupContent(4, "\n    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "col-md-4");
            __builder.AddMarkupContent(7, "\n        ");
            __builder.OpenElement(8, "form");
            __builder.AddMarkupContent(9, "\n            ");
            __builder.OpenElement(10, "div");
            __builder.AddAttribute(11, "class", "form-group");
            __builder.AddMarkupContent(12, "\n                ");
            __builder.AddMarkupContent(13, "<label for=\"Name\" class=\"control-label\">Name</label>\n                ");
            __builder.OpenElement(14, "input");
            __builder.AddAttribute(15, "for", "Name");
            __builder.AddAttribute(16, "class", "form-control");
            __builder.AddAttribute(17, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 13 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                               emp.Name

#line default
#line hidden
            ));
            __builder.AddAttribute(18, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Name = __value, emp.Name));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(20, "\n            ");
            __builder.OpenElement(21, "div");
            __builder.AddAttribute(22, "class", "form-group");
            __builder.AddMarkupContent(23, "\n                ");
            __builder.AddMarkupContent(24, "<label asp-for=\"Designation\" class=\"control-label\">Designation</label>\n                ");
            __builder.OpenElement(25, "input");
            __builder.AddAttribute(26, "for", "Designation");
            __builder.AddAttribute(27, "class", "form-control");
            __builder.AddAttribute(28, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 17 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                                      emp.Designation

#line default
#line hidden
            ));
            __builder.AddAttribute(29, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Designation = __value, emp.Designation));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(30, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\n            ");
            __builder.OpenElement(32, "div");
            __builder.AddAttribute(33, "class", "form-group");
            __builder.AddMarkupContent(34, "\n                ");
            __builder.AddMarkupContent(35, "<label asp-for=\"Email\" class=\"control-label\">Email</label>\n                ");
            __builder.OpenElement(36, "input");
            __builder.AddAttribute(37, "asp-for", "Email");
            __builder.AddAttribute(38, "class", "form-control");
            __builder.AddAttribute(39, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 21 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                                    emp.Email

#line default
#line hidden
            ));
            __builder.AddAttribute(40, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Email = __value, emp.Email));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(42, "\n            ");
            __builder.OpenElement(43, "div");
            __builder.AddAttribute(44, "class", "form-group");
            __builder.AddMarkupContent(45, "\n                ");
            __builder.AddMarkupContent(46, "<label asp-for=\"Location\" class=\"control-label\">Location</label>\n                ");
            __builder.OpenElement(47, "input");
            __builder.AddAttribute(48, "asp-for", "Location");
            __builder.AddAttribute(49, "class", "form-control");
            __builder.AddAttribute(50, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 25 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                                       emp.Location

#line default
#line hidden
            ));
            __builder.AddAttribute(51, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Location = __value, emp.Location));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(52, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(53, "\n            ");
            __builder.OpenElement(54, "div");
            __builder.AddAttribute(55, "class", " form-group");
            __builder.AddMarkupContent(56, "\n                ");
            __builder.AddMarkupContent(57, "<label asp-for=\"Phone\" class=\"control-label\">Phone</label>\n                ");
            __builder.OpenElement(58, "input");
            __builder.AddAttribute(59, "asp-for", "Phone");
            __builder.AddAttribute(60, "class", "form-control");
            __builder.AddAttribute(61, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 29 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                                    emp.PhoneNumber

#line default
#line hidden
            ));
            __builder.AddAttribute(62, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.PhoneNumber = __value, emp.PhoneNumber));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(63, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(64, "\n            ");
            __builder.OpenElement(65, "div");
            __builder.AddAttribute(66, "class", "form-group");
            __builder.AddMarkupContent(67, "\n                ");
            __builder.OpenElement(68, "input");
            __builder.AddAttribute(69, "type", "submit");
            __builder.AddAttribute(70, "value", "Save");
            __builder.AddAttribute(71, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 32 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                              async () => await UpdateEmployee()

#line default
#line hidden
            ));
            __builder.AddAttribute(72, "class", "btn btn-default");
            __builder.CloseElement();
            __builder.AddMarkupContent(73, "\n                ");
            __builder.OpenElement(74, "input");
            __builder.AddAttribute(75, "type", "submit");
            __builder.AddAttribute(76, "value", "Cancel");
            __builder.AddAttribute(77, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 33 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
                                                               cancel

#line default
#line hidden
            ));
            __builder.AddAttribute(78, "class", "btn");
            __builder.CloseElement();
            __builder.AddMarkupContent(79, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(80, "\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(81, "\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(82, "\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#line 39 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\EditEmployee.razor"
       

    [Parameter]
    public string empId { get; set; }

    Employee emp = new Employee();

    protected override async Task OnInitializedAsync()
    {
        emp = await Http.GetJsonAsync<Employee>("/api/Employee/Details/" + Convert.ToInt64(empId));
    }

    protected async Task UpdateEmployee()
    {
        await Http.SendJsonAsync(HttpMethod.Put, "api/Employee/Edit", emp);
        navigation.NavigateTo("/employee");

    }

    void cancel()
    {
        navigation.NavigateTo("/employee");

    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Microsoft.AspNetCore.Components.NavigationManager navigation { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
