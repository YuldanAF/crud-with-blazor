#pragma checksum "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2096117e80f3e51510cbce08e7cee1b85170d730"
// <auto-generated/>
#pragma warning disable 1591
namespace BlazorCrud.Client.Pages.Employee
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 2 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 3 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 4 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 6 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using BlazorCrud.Client;

#line default
#line hidden
#line 7 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using BlazorCrud.Client.Shared;

#line default
#line hidden
#line 8 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\_Imports.razor"
using BlazorCrud.Shared.Models;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.RouteAttribute("/employee/add")]
    public partial class AddEmployee : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h1>Create</h1>\n\n<hr>\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "row");
            __builder.AddMarkupContent(3, "\n    ");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "col-md-4");
            __builder.AddMarkupContent(6, "\n        ");
            __builder.OpenElement(7, "form");
            __builder.AddMarkupContent(8, "\n            ");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "form-group");
            __builder.AddMarkupContent(11, "\n                ");
            __builder.AddMarkupContent(12, "<label for=\"Name\" class=\"control-label\">Name</label>\n                ");
            __builder.OpenElement(13, "input");
            __builder.AddAttribute(14, "for", "Name");
            __builder.AddAttribute(15, "class", "form-control");
            __builder.AddAttribute(16, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 13 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                                               emp.Name

#line default
#line hidden
            ));
            __builder.AddAttribute(17, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Name = __value, emp.Name));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\n            ");
            __builder.OpenElement(20, "div");
            __builder.AddAttribute(21, "class", "form-group");
            __builder.AddMarkupContent(22, "\n                ");
            __builder.AddMarkupContent(23, "<label asp-for=\"Designation\" class=\"control-label\">Designation</label>\n                ");
            __builder.OpenElement(24, "input");
            __builder.AddAttribute(25, "for", "Designation");
            __builder.AddAttribute(26, "class", "form-control");
            __builder.AddAttribute(27, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 17 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                                                      emp.Designation

#line default
#line hidden
            ));
            __builder.AddAttribute(28, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Designation = __value, emp.Designation));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(29, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(30, "\n            ");
            __builder.OpenElement(31, "div");
            __builder.AddAttribute(32, "class", "form-group");
            __builder.AddMarkupContent(33, "\n                ");
            __builder.AddMarkupContent(34, "<label asp-for=\"Email\" class=\"control-label\">Email</label>\n                ");
            __builder.OpenElement(35, "input");
            __builder.AddAttribute(36, "asp-for", "Email");
            __builder.AddAttribute(37, "class", "form-control");
            __builder.AddAttribute(38, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 21 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                                                   emp.Email

#line default
#line hidden
            ));
            __builder.AddAttribute(39, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Email = __value, emp.Email));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(40, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\n            ");
            __builder.OpenElement(42, "div");
            __builder.AddAttribute(43, "class", "form-group");
            __builder.AddMarkupContent(44, "\n                ");
            __builder.AddMarkupContent(45, "<label asp-for=\"Location\" class=\"control-label\">Location</label>\n                ");
            __builder.OpenElement(46, "input");
            __builder.AddAttribute(47, "asp-for", "Location");
            __builder.AddAttribute(48, "class", "form-control");
            __builder.AddAttribute(49, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 25 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                                                       emp.Location

#line default
#line hidden
            ));
            __builder.AddAttribute(50, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.Location = __value, emp.Location));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(51, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(52, "\n            ");
            __builder.OpenElement(53, "div");
            __builder.AddAttribute(54, "class", "form-group");
            __builder.AddMarkupContent(55, "\n                ");
            __builder.AddMarkupContent(56, "<label asp-for=\"Phone\" class=\"control-label\">Phone</label>\n                ");
            __builder.OpenElement(57, "input");
            __builder.AddAttribute(58, "asp-for", "Phone");
            __builder.AddAttribute(59, "class", "form-control");
            __builder.AddAttribute(60, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 29 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                                                   emp.PhoneNumber

#line default
#line hidden
            ));
            __builder.AddAttribute(61, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => emp.PhoneNumber = __value, emp.PhoneNumber));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(62, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(63, "\n            ");
            __builder.OpenElement(64, "div");
            __builder.AddAttribute(65, "class", "form-group");
            __builder.AddMarkupContent(66, "\n                ");
            __builder.OpenElement(67, "button");
            __builder.AddAttribute(68, "type", "submit");
            __builder.AddAttribute(69, "class", "btn btn-default");
            __builder.AddAttribute(70, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 32 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                                                          async () => await CreateEmployee()

#line default
#line hidden
            ));
            __builder.AddContent(71, "Save");
            __builder.CloseElement();
            __builder.AddMarkupContent(72, "\n                ");
            __builder.OpenElement(73, "button");
            __builder.AddAttribute(74, "class", "btn");
            __builder.AddAttribute(75, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 33 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
                                               cancel

#line default
#line hidden
            ));
            __builder.AddContent(76, "Cancel");
            __builder.CloseElement();
            __builder.AddMarkupContent(77, "\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(78, "\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(79, "\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(80, "\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#line 39 "C:\Users\hilmi\source\repos\employee-management-blazor-master\employee-management-blazor-master\BlazorCrud\BlazorCrud\Client\Pages\Employee\AddEmployee.razor"
       

    Employee emp = new Employee();

    protected async Task CreateEmployee()
    {
        await Http.SendJsonAsync(HttpMethod.Post, "/api/Employee/Create", emp);
        navigation.NavigateTo("/employee");
    }

    void cancel()
    {
        navigation.NavigateTo("/employee");
    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Microsoft.AspNetCore.Components.NavigationManager navigation { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
